package com.tripolis.export_service;

import io.dropwizard.Configuration;

import java.util.Set;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExportServiceConfiguration extends Configuration {
	
	@NotEmpty
	private String oauthKey;
	@NotEmpty
	private String oauthSecret;
	
	@NotEmpty
	private String urlContactApi;
	@NotEmpty
	private String urlAdminApi;
	@NotEmpty
	private String urlEventsApi;
	
	@NotEmpty
	private Set<String> elasticsearchHosts;
	
	@NotEmpty
	private String elasticsearchClusterName;

	@JsonProperty
	public String getOauthKey() {
		return oauthKey;
	}

	@JsonProperty
	public void setOauthKey(String oauthKey) {
		this.oauthKey = oauthKey;
	}

	@JsonProperty
	public String getOauthSecret() {
		return oauthSecret;
	}

	@JsonProperty
	public void setOauthSecret(String oauthSecret) {
		this.oauthSecret = oauthSecret;
	}

	@JsonProperty
	public String getUrlContactApi() {
		return urlContactApi;
	}

	@JsonProperty
	public void setUrlContactApi(String urlContactApi) {
		this.urlContactApi = urlContactApi;
	}

	@JsonProperty
	public String getUrlAdminApi() {
		return urlAdminApi;
	}
	@JsonProperty
	public void setUrlAdminApi(String urlAdminApi) {
		this.urlAdminApi = urlAdminApi;
	}

	@JsonProperty
	public Set<String> getElasticsearchHosts() {
		return elasticsearchHosts;
	}
	@JsonProperty
	public void setElasticsearchHosts(Set<String> elasticsearchHosts) {
		this.elasticsearchHosts = elasticsearchHosts;
	}

	@JsonProperty
	public String getUrlEventsApi() {
		return urlEventsApi;
	}
	@JsonProperty
	public void setUrlEventsApi(String urlEventsApi) {
		this.urlEventsApi = urlEventsApi;
	}

	@JsonProperty
	public String getElasticsearchClusterName() {
		return elasticsearchClusterName;
	}
	@JsonProperty
	public void setElasticsearchClusterName(String elasticsearchClusterName) {
		this.elasticsearchClusterName = elasticsearchClusterName;
	}
	
}
