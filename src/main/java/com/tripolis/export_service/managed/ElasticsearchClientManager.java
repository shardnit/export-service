package com.tripolis.export_service.managed;

import io.dropwizard.lifecycle.Managed;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.RestRequest.Method;
import org.elasticsearch.rest.action.search.RestSearchAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;

public class ElasticsearchClientManager implements Managed {

	private final Client client;
	private static final Logger LOGGER = LoggerFactory.getLogger(ElasticsearchClientManager.class);
	public ElasticsearchClientManager(String clusterName, Set<String> esHosts) {
		LOGGER.info("Creating Elasticsearch client for cluster: "+ clusterName +", and hosts: "+esHosts);
		Settings settings = ImmutableSettings.settingsBuilder()
				.put("client.transport.sniff", true)
				.put("cluster.name", clusterName).build();
		final Client transportClient = new TransportClient(settings);
		for(String s: esHosts) {
			((TransportClient) transportClient).addTransportAddress(new InetSocketTransportAddress(s, 9300));
		}
		this.client = transportClient;
	}

	public void start() throws Exception {
		ClusterHealthResponse healthResponse = client.admin().cluster().prepareHealth().setWaitForYellowStatus().setTimeout("10s").execute().actionGet();
		if(healthResponse.isTimedOut()) {
			throw new TimeoutException("Elaticsearch cluster health could not achieve YELLOW before timeout");
		}
	}

	public void stop() throws Exception {
		client.close();
	}
	
	public ClusterHealthResponse health() {
		return this.client.admin().cluster().prepareHealth().execute().actionGet();
	}
	
	public void scrollAndScan(String index, String type, String query, int bounceThreshold) {
		RestRequest restRequest = new RestRequest() {
			
			@Override
			public String param(String arg0, String arg1) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String uri() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String rawPath() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Map<String, String> params() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String param(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Method method() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Iterable<Entry<String, String>> headers() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String header(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasParam(String arg0) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean hasContent() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean contentUnsafe() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public BytesReference content() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		restRequest.params().put("index", index);
		
		SearchRequestBuilder searchRequest = client.prepareSearch(index)
        .setSearchType(SearchType.SCAN)
        .setScroll(new TimeValue(60 * 5* 1000))
        .setQuery(QueryBuilders.matchAllQuery())
        .setPostFilter(getFilters(bounceThreshold))
        .setSize(1000); //1000 hits per shard will be returned for each scroll
		
		LOGGER.info("Running Elasticsearch scroll query: "+searchRequest.toString());
		SearchResponse scrollResponse = searchRequest.execute().actionGet(); 
	}
	
	private FilterBuilder getFilters(int bounceThreshold) {
		final JsonNodeFactory factory = JsonNodeFactory.instance;
		factory.objectNode().put("query","Contacts::EmailAddress").put("operator","and");
		
		FilterBuilder filter = FilterBuilders.boolFilter()
				.must(FilterBuilders.termFilter("channels.hard_bounced", "false"))
				.must(FilterBuilders.termFilter("channels.primary", "true"))
				.must(FilterBuilders.rangeFilter("channels.soft_bounced_count").lte(bounceThreshold))
				.must(FilterBuilders.queryFilter(QueryBuilders.matchQuery("channels._type", factory)));
		LOGGER.info("Elasticsearch filter: "+filter.toString());
		return filter;
	}

}
