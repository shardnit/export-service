package com.tripolis.export_service.managed;

import io.dropwizard.lifecycle.Managed;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tripolis.export_service.core.TaskRejectedHandler;

public class LocalExecutorServiceManager implements Managed{

	private final ExecutorService exportExecutor;
	private final ExecutorService groupDetailsExecutor;
	private static final Logger LOGGER = LoggerFactory.getLogger(LocalExecutorServiceManager.class);
	
	public LocalExecutorServiceManager(ExecutorService exportExecutor, ExecutorService groupDetailsExecutor) {
		this.exportExecutor = exportExecutor;
		this.groupDetailsExecutor = groupDetailsExecutor;
	}
	@Override
	public void start() throws Exception {
		// OK BOSS
	}

	@Override
	public void stop() throws Exception {
		// no other place to await termination of executor service. doing it here
		// dropwizard doesnt provide a hook for modifying the shutdown behaviour.
		// http://www.dropwizard.io/0.7.0/dropwizard-lifecycle/apidocs/io/dropwizard/lifecycle/setup/ExecutorServiceBuilder.html
		// https://github.com/dropwizard/dropwizard/blob/master/dropwizard-lifecycle/src/main/java/io/dropwizard/lifecycle/ExecutorServiceManager.java#L26
		LOGGER.info("Shutting down export executor service..");
		this.exportExecutor.shutdown();
		// Do not exit until all the tasks are processed
		while(!this.exportExecutor.awaitTermination(10, TimeUnit.SECONDS)) {
			LOGGER.info("Awaiting completion of tasks in export executor - "+TaskRejectedHandler.executorStatus((ThreadPoolExecutor) this.exportExecutor, null));
		}

		LOGGER.info("Shutting down group details executor service..");
		this.groupDetailsExecutor.shutdown();
		// Do not exit until all the tasks are processed
		while(!this.groupDetailsExecutor.awaitTermination(10, TimeUnit.SECONDS)) {
			LOGGER.info("Awaiting completion of tasks in group details executor - "+TaskRejectedHandler.executorStatus((ThreadPoolExecutor) this.groupDetailsExecutor, null));
		}
	}

}
