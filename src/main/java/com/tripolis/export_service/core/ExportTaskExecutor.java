package com.tripolis.export_service.core;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tripolis.export_service.ExportServiceConfiguration;
import com.tripolis.export_service.managed.ElasticsearchClientManager;

public class ExportTaskExecutor implements Runnable {

	private final RequestBody data;
	private final ExecutorService groupDetailsExecutor;
	private final ExportServiceConfiguration config;
	private final Logger LOGGER = LoggerFactory.getLogger(ExportTaskExecutor.class);
	private final ElasticsearchClientManager client;
	
	public ExportTaskExecutor(ExportServiceConfiguration config, RequestBody body, ExecutorService service, ElasticsearchClientManager client) {
		this.data = body;
		this.groupDetailsExecutor = service;
		this.config = config;
		this.client = client;
	}
	public void run() {
		// get the auth token first
		Future<String> future = groupDetailsExecutor.submit(new GroupDetailsExecutor(config.getUrlAdminApi(), config.getOauthKey(), config.getOauthSecret()
				, config.getUrlContactApi(), data));
		
		try {
			String groupDefinition= future.get();
			LOGGER.info("Group definition received from group details executor: "+groupDefinition);
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Exception while waiting for group details future", e);
		}
	}

}
