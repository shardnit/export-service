package com.tripolis.export_service.core;

import java.util.concurrent.Callable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

public class GroupDetailsExecutor implements Callable<String> {

	private final String urlAdminApi;
	private final String key;
	private final String secret;
	private final String urlContactApi;
	private final RequestBody data;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GroupDetailsExecutor.class);

	public GroupDetailsExecutor(String urlAdminApi, String key, String secret,
			String urlContactApi, RequestBody data) {
		this.urlAdminApi = urlAdminApi;
		this.secret = secret;
		this.key = key;
		this.urlContactApi = urlContactApi;
		this.data = data;
	}

	@Override
	public String call() throws Exception {
		String token = AuthTokenSingleton.getInstance(urlAdminApi, key, secret)
				.getAuthToken();
		String groupDefinition = getGroupDetails(token);
		return groupDefinition;
	}

	private String getGroupDetails(String token) {
		String endpoint = urlContactApi
				+ String.format("/private/tenants/%s/groups/%s",
						this.data.getTenantUUID(), this.data.getGroupId());
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(endpoint);
		Invocation.Builder invocationBuilder = target
				.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("Authorization", "Bearer " + token);
		Response response = invocationBuilder.post(null);
		LOGGER.info("Response from contact api: " + response.toString());
		if (response.getStatus() == 401) {
			// retry
			return null;
		} else if (response.getStatus() == 200) {
			JsonNode body = response.readEntity(JsonNode.class);
			LOGGER.info("Response body from contact api: " + body.toString());
			return body.get("definition").asText();
		} else {
			return null;
		}
	}

}
