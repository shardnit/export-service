package com.tripolis.export_service.core;

import io.dropwizard.jackson.JsonSnakeCase;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

@JsonSnakeCase
public class RequestBody {
	
	@NotEmpty
	private String tenantUUID;
	@NotEmpty
	private String campaignId;
	@NotEmpty
	private String nodeId;
	@NotEmpty
	private String groupId;
	@NotNull
	private Integer pageSize;
	@NotEmpty
	private String[] mergeFields;
	@NotEmpty
	private int bounceThreshold;
	@NotEmpty
	private String s3Bucket;
	@NotEmpty
	private String s3FilePath;
	@NotEmpty
	private String exportFileName;
	@NotEmpty
	private String s3Region;
	
	@JsonProperty
	public String getTenantUUID() {
		return tenantUUID;
	}
	@JsonProperty
	public void setTenantUUID(String tenantUUID) {
		this.tenantUUID = tenantUUID;
	}
	
	@JsonProperty
	public String getCampaignId() {
		return campaignId;
	}
	@JsonProperty
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
	@JsonProperty
	public String getNodeId() {
		return nodeId;
	}
	@JsonProperty
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	@JsonProperty
	public String getGroupId() {
		return groupId;
	}
	@JsonProperty
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	@JsonProperty
	public int getPageSize() {
		return pageSize;
	}
	@JsonProperty
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	@JsonProperty
	public String[] getMergeFields() {
		return mergeFields;
	}
	@JsonProperty
	public void setMergeFields(String[] mergeFields) {
		this.mergeFields = mergeFields;
	}
	
	@JsonProperty
	public int getBounceThreshold() {
		return bounceThreshold;
	}
	@JsonProperty
	public void setBounceThreshold(int bounceThreshold) {
		this.bounceThreshold = bounceThreshold;
	}
	
	@JsonProperty
	public String getS3Bucket() {
		return s3Bucket;
	}
	@JsonProperty
	public void setS3Bucket(String s3Bucket) {
		this.s3Bucket = s3Bucket;
	}
	
	@JsonProperty
	public String getS3FilePath() {
		return s3FilePath;
	}
	@JsonProperty
	public void setS3FilePath(String s3FilePath) {
		this.s3FilePath = s3FilePath;
	}
	
	@JsonProperty
	public String getExportFileName() {
		return exportFileName;
	}
	@JsonProperty
	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}
	
	@JsonProperty
	public String getS3Region() {
		return s3Region;
	}
	@JsonProperty
	public void setS3Region(String s3Region) {
		this.s3Region = s3Region;
	}
	
}
