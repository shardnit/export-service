package com.tripolis.export_service.core;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class AuthTokenSingleton {

	private static AuthTokenSingleton singleton;
	private volatile String authToken;
	private final String key;
	private final String secret;
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthTokenSingleton.class);
	private static final Client client = ClientBuilder.newClient();
	private final WebTarget target;
	private final Invocation.Builder invocationBuilder;
	public AuthTokenSingleton(String urlAdminApi, String key, String secret) {
		this.secret = secret;
		this.key = key;
		this.target = client.target(urlAdminApi+"/oauth/token?grant_type=client_credentials");
		this.invocationBuilder = target.request(MediaType.APPLICATION_JSON);
	}
	
	public static AuthTokenSingleton getInstance(String urlAdminApi, String key, String secret) {
		if(singleton == null) {
			singleton = new AuthTokenSingleton(urlAdminApi, key, secret);
		}
		return singleton;
	}
	
	public String getAuthToken() {
		if(authToken == null || authToken.isEmpty()) {
			authToken = getAuthTokenFromAdminApi();
		}
		return authToken;
	}
	
	public void invalidateAuthToken() {
		this.authToken = "";
	}
	
	private String getAuthTokenFromAdminApi() {
		Response response = invocationBuilder.header("Authorization", "Basic "+base64EncodedCredentials()).post(null);
		LOGGER.info("Response from admin api: "+response.toString()+", status: "+response.getStatusInfo());
		if(response.getStatus() != 200) {
			LOGGER.warn("Got unsuccessful token response from admin api. Please recheck client key and secret");
			return null;
		}
		JsonNode body = response.readEntity(JsonNode.class);
		LOGGER.info("Response body: "+body);
		JsonNode innerBody = body.get("body");
		LOGGER.info("Inner body: "+innerBody.asText());
		
		JsonNode token;
		try {
			token = new ObjectMapper().readValue(innerBody.asText().getBytes(), JsonNode.class).get("access_token");
			LOGGER.info("token: "+token.asText());
			return token.asText();
		} catch (IOException e) {
			LOGGER.error("Error while parsing admin api response", e);
			return null;
		}
	}
	
	private String base64EncodedCredentials() {
		return Base64.encodeAsString(key+":"+secret);
	}
}
