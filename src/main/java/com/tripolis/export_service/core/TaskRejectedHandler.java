package com.tripolis.export_service.core;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TaskRejectedHandler implements RejectedExecutionHandler{

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskRejectedHandler.class);
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		String status = TaskRejectedHandler.executorStatus(executor, "Task queue is full. Please try again.");
		if (status != null) {
			throw new WebApplicationException(Response.status(Status.NOT_ACCEPTABLE).entity(status).
					type(MediaType.APPLICATION_JSON).build());
		} else {
			status = "{\"message\": \"Task queue is full. Please try again\"}";
			throw new WebApplicationException(Response.status(Status.NOT_ACCEPTABLE).entity(status).
					type(MediaType.APPLICATION_JSON).build());
		}
	}
	
	public static String executorStatus(ThreadPoolExecutor executor, String errorMessage) {
		Map<String, Object> map = new HashMap<>();
		map.put("thread_pool_size", executor.getPoolSize());
		map.put("active_threads", executor.getActiveCount());
		map.put("queued_tasks", executor.getQueue().size());
		map.put("completed_tasks", executor.getCompletedTaskCount());
		if(errorMessage != null && !errorMessage.isEmpty())
			map.put("error_message", errorMessage);
		try {
			return new ObjectMapper().writeValueAsString(map);
		} catch (JsonProcessingException e) {
			LOGGER.error("Error while creating executor status json", e);
			return null;
		}
	}

}
