package com.tripolis.export_service.resources;

import java.util.concurrent.ExecutorService;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tripolis.export_service.ExportServiceConfiguration;
import com.tripolis.export_service.core.ExportTaskExecutor;
import com.tripolis.export_service.core.RequestBody;
import com.tripolis.export_service.managed.ElasticsearchClientManager;

@Path("/export")
@Produces(MediaType.APPLICATION_JSON)
public class ExportServiceResource {

	private final ExecutorService exportExecutor;
	private final ExecutorService groupDetailsExecutor;
	private final ExportServiceConfiguration config;
	private final ElasticsearchClientManager client;
	
	public ExportServiceResource(ExportServiceConfiguration config, ExecutorService exportService, ExecutorService groupDetailsService, ElasticsearchClientManager client) {
		this.exportExecutor = exportService;
		this.groupDetailsExecutor = groupDetailsService;
		this.config = config;
		this.client = client;
	}
	@POST
	public Response startExport(@Valid RequestBody request) {
		exportExecutor.submit(new ExportTaskExecutor(config, request, groupDetailsExecutor, client));
		return Response.noContent().build();
	}
}
