package com.tripolis.export_service;

import io.dropwizard.Application;
import io.dropwizard.lifecycle.setup.ExecutorServiceBuilder;
import io.dropwizard.setup.Environment;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tripolis.export_service.core.TaskRejectedHandler;
import com.tripolis.export_service.health.ElasticsearchHealthCheck;
import com.tripolis.export_service.health.ExportHealthCheck;
import com.tripolis.export_service.managed.ElasticsearchClientManager;
import com.tripolis.export_service.managed.LocalExecutorServiceManager;
import com.tripolis.export_service.resources.ExportServiceResource;

public class ExportServiceApplication extends Application<ExportServiceConfiguration>{
	private static final int CORES = Runtime.getRuntime().availableProcessors();
	private static final int POOL = 2*CORES + 2;
	private static final int CAPACITY = 1000; // atmost 1000 pending export jobs
	private static final Logger LOGGER = LoggerFactory.getLogger(ExportServiceApplication.class);

	public static void main(String[] args) throws Exception {
		new ExportServiceApplication().run(args);
	}
	@Override
	public void run(ExportServiceConfiguration config, Environment env)
			throws Exception {
		final ExecutorService exportExecutor = createExportExecutorService(env);
		final ExecutorService groupDetailsExecutor = createGroupDetailsExecutorService(env);
		
		final LocalExecutorServiceManager executorManager = new LocalExecutorServiceManager(exportExecutor, groupDetailsExecutor);
		env.lifecycle().manage(executorManager);

		final ElasticsearchClientManager client = new ElasticsearchClientManager(config.getElasticsearchClusterName(), config.getElasticsearchHosts());
		env.lifecycle().manage(client);
		
		final ElasticsearchHealthCheck esHealth = new ElasticsearchHealthCheck(client);
		env.healthChecks().register("elasticsearch", esHealth);
		
		final ExportServiceResource resource = new ExportServiceResource(config, exportExecutor, groupDetailsExecutor, client);
		env.jersey().register(resource);
		
		final ExportHealthCheck exportHealth = new ExportHealthCheck(exportExecutor);
		env.healthChecks().register("export", exportHealth);
		
	}
	
	private ExecutorService createExportExecutorService(Environment env) {
		final ExecutorServiceBuilder execBuilder = env.lifecycle().executorService("exportTask");
		LOGGER.info("Starting a thread pool for export tasks of size: "+POOL);
		execBuilder.maxThreads(POOL);
		LOGGER.info("Starting a task pool for export tasks of size: "+CAPACITY);
		execBuilder.workQueue(new ArrayBlockingQueue<Runnable>(CAPACITY));
		execBuilder.rejectedExecutionHandler(new TaskRejectedHandler());
		return execBuilder.build();
	}
	
	private ExecutorService createGroupDetailsExecutorService(Environment env) {
		final ExecutorServiceBuilder execBuilder = env.lifecycle().executorService("groupDetailsTask");
		LOGGER.info("Starting a thread pool for group detail tasks of size: "+1);
		// only 1 thread in group details pool since we want to serialize the token retrieval
		execBuilder.maxThreads(1);
		LOGGER.info("Starting a task pool for group detail tasks of size: "+POOL);
		execBuilder.workQueue(new ArrayBlockingQueue<Runnable>(POOL));
		execBuilder.rejectedExecutionHandler(new TaskRejectedHandler());
		return execBuilder.build();
	}
}
