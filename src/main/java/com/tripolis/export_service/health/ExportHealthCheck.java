package com.tripolis.export_service.health;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

import com.codahale.metrics.health.HealthCheck;
import com.tripolis.export_service.core.TaskRejectedHandler;

public class ExportHealthCheck extends HealthCheck {

	private final ExecutorService service;
	public ExportHealthCheck(ExecutorService service) {
		this.service = service;
	}
	@Override
	protected Result check() throws Exception {
		// we can safely cast to thread pool executor
		// https://github.com/dropwizard/dropwizard/blob/master/dropwizard-lifecycle/src/main/java/io/dropwizard/lifecycle/setup/ExecutorServiceBuilder.java#L76
		return Result.healthy(TaskRejectedHandler.executorStatus((ThreadPoolExecutor) service, null));
	}
}
