package com.tripolis.export_service.health;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;

import com.codahale.metrics.health.HealthCheck;
import com.tripolis.export_service.managed.ElasticsearchClientManager;

public class ElasticsearchHealthCheck extends HealthCheck{
	
	private final ElasticsearchClientManager client;
	
	public ElasticsearchHealthCheck(ElasticsearchClientManager client) {
		this.client = client;
	}

	@Override
	protected Result check() throws Exception {
		ClusterHealthResponse response = this.client.health();
		return Result.healthy(response.toString());
	}

}
