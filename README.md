# Export Service

This is a REST service, consisting of only `export` endpoint, to collect contacts from Elasticsearch based on the group definition (elasticsearch query) provided. Please take a look at `RequestBody` class to see what `export` endpoint expects in request body. Also, the application bootstrap process requires a config (yml) file with some compulsory variables. Please consult `ExportServiceConfiguration` class.

## Notes

1. Every api request to `export` endpoint results in a new export task being submitted to [ExecutorService](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html). We use a [ThreadPoolExecutor](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ThreadPoolExecutor.html) implementation with bounded queue and thread pool.
2. Since we use a bounded task queue and thread pool, in the situation when all threads are busy and task queue is full, requests will be denied until there is room for more. Its the responsibility of the client to retry (or check the status and then retry).
3. When `kill` signal sent to this service, it will wait for all the pending tasks to be finished. Avoid sending `kill -9` otherwise all pending export tasks will be thrown away without processing.
4. Moreover, in the event when service is waiting for pending tasks to be finished (after shutdown signal) it wont accept any new requests.

## Health Check

You can check status of this service at `http://<url>:8081/healthcheck`.
